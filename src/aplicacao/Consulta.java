package aplicacao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import fachada.Sistema;
import modelo.Discente;
import modelo.Participante;
import modelo.Projeto;
import modelo.Servidor;

public class Consulta {

	private static EntityManager manager;
	
	public static void main(String args[]) {
		
		Sistema.inicializar();
		// Lista projetos por meio de um Edital
		Query q = manager.createQuery("SELECT e.projetos from Edital e where e.nome LIKE 'Edital de testes 10'");
		List<Projeto> projetos = q.getResultList();
		
		System.out.println("\nListando Projetos por meio de um edital");
		for (Projeto p : projetos) {
			System.out.println(p);
		}
		
		//Lista Participantes por meio de um Projeto
		Query q2 = manager.createQuery("SELECT p.participantes from Projeto p where p.titulo LIKE 'Projeto de testes 1'");
		List<Participante> participantes = q2.getResultList();
		
		System.out.println("\nListando participantes de um Projeto");
		for (Participante pa : participantes) {
			System.out.println(pa);
		}
		
		//Lista Discentes por meio de um curso
		Query q3 = manager.createQuery("SELECT c.discentes from Curso c where c.nome LIKE 'TSI'");
		List<Discente> discentes = q3.getResultList();
		
		System.out.println("\nListando Discentes de um CURSO");
		for (Discente d : discentes) {
			System.out.println(d);
		}
		
		//Lista Projeto de um participante
		Query q4 = manager.createQuery("SELECT pa.projetos from Participante pa where pa.nome LIKE 'Aluno 1'");
		List<Projeto> projetosd = q4.getResultList();
		
		System.out.println("\nListando Projetos de um Discente");
		for (Projeto pro : projetosd) {
			System.out.println(pro);
		}
		
		
		//Lista os coordenadores dos projetos
		Query q5 = manager.createQuery("SELECT ser FROM Projeto p JOIN FETCH p.participantes pa JOIN Servidor ser WHERE ser.coordenador = true");
		List<Participante> coordenadores = q5.getResultList();
		
		System.out.println("\nListando Coordenadores de projetos");
		for (Participante coordenador : coordenadores) {
			System.out.println(coordenador);
		}
		
	}
	
	
}
