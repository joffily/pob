package aplicacao;
import fachada.Sistema;

public class Teste {

	public static void main(String[] args) {

		Sistema.inicializar();
		System.out.println("Nada a se fazer.. Utilize as classes Cadastro, Atualização e Consulta");
		Sistema.fechar();
	}

}
