package aplicacao;

import fachada.Sistema;
import modelo.Discente;
import modelo.Edital;
import modelo.Projeto;

public class Atualizacao {

	public static void main(String[] args) {
		//Iniciando acesso ao banco
		Sistema.inicializar();
		
		//Listar todos os objetos do banco
		System.out.println(Sistema.listarEditais());
		System.out.println(Sistema.listarProjetos());
		System.out.println(Sistema.listarParticipantes());
		System.out.println(Sistema.listarCursos());
		
		adicionarERemoverParticipacao();
		removerEditalCascade();
		
		//Fechando conexão
		Sistema.fechar();
	}
	
	private static void adicionarERemoverParticipacao() {
		try {
			Edital e = Sistema.buscarEdital("Edital de testes 001");
			Projeto p = Sistema.buscarProjeto("Projeto de testes 1");
			Discente dis = Sistema.buscaDiscente(1000);
			
			if (Sistema.removerParticipanteProjeto(p, dis)) {
				System.out.println("Removendo: " + dis + ", do projeto: " + p);
				System.out.println("Participante removido com sucesso.");
			}
			
			if (Sistema.addParticipanteProjeto(p, dis)) {
				System.out.println("Adicionando: " + dis + ", do projeto: " + p);	
				System.out.println("Participante adicionado com sucesso!");
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void removerEditalCascade() {
		try {
			System.out.println(Sistema.removerEdital("Edital de testes 002"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
