package aplicacao;

import java.util.List;

//import daojpa.*;
import daodb4o.*;
import modelo.*;
import fachada.Sistema;

public class Cadastro {
	private static DAOProjeto daoprojeto = new DAOProjeto();
	private static DAOParticipante daoparticipante = new DAOParticipante();
	private static DAOCurso daocurso = new DAOCurso();
	private static DAOTipoParticipacao daotipoparticipacao = new DAOTipoParticipacao();
	private static DAODiscente daodiscente = new DAODiscente();
	private static DAOServidor daoservidor = new DAOServidor();
	private static DAOEdital daoedital = new DAOEdital();

	public static void main(String[] args) {
		//Iniciando acesso ao banco
		DAO.abrir();
		Cadastro.cargaDiscentes();
		Cadastro.cargaServidor();
		Cadastro.cargaEdital();
		Cadastro.cargaProjeto();
		Cadastro.addParticipantes();
		//Fechando conexão
		DAO.fechar();
	}
	
	public static void addParticipantes() {
		List<Participante> participantes = daoparticipante.listar();
		List<Projeto> projetos = daoprojeto.listar();

		DAO.begin();
		Projeto projeto = projetos.get(0);

		for (Participante participante : participantes) {
			projeto.addParticipante(participante);
			daoprojeto.atualizar(projeto);
			System.out.println("Adicionando " + participante + ", ao projeto: " + projeto);
		}
		DAO.commit();
	}
	
	// DAO OK
	public static void cargaCurso() {
		try {
			DAO.begin();
			Curso curso = new Curso("TSI", "Superior");
			daocurso.persistir(curso);
			DAO.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// DAO OK
	public static void cargaTipos() {
		try {
			DAO.begin();
			TipoParticipacao bolsista = new TipoParticipacao("Bolsista");
			TipoParticipacao voluntario = new TipoParticipacao("Voluntário");
			daotipoparticipacao.persistir(bolsista);
			daotipoparticipacao.persistir(voluntario);
			DAO.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// DAO OK
	public static void cargaDiscentes() {
		Cadastro.cargaTipos();
		Cadastro.cargaCurso();

		Curso curso = daocurso.getBynome("TSI");
		TipoParticipacao tipo = daotipoparticipacao.getByTipo("Voluntário");
		
		try {
			Discente d1 = Sistema.cadastrarDiscente("Aluno 13", tipo, curso, 9000);	
			Discente d2 = Sistema.cadastrarDiscente("Aluno 2", tipo, curso, 2000);
			Discente d3 = Sistema.cadastrarDiscente("Aluno 3", tipo, curso, 3000);
			Discente d4 = Sistema.cadastrarDiscente("Aluno 4", tipo, curso, 4000);
			
			System.out.println("Adicionado Discente: " + d1);
			System.out.println("Adicionado Discente: " + d2);
			System.out.println("Adicionado Discente: " + d3);
			System.out.println("Adicionado Discente: " + d4);

			DAO.begin();
			daodiscente.persistir(d1);
			daodiscente.persistir(d2);
			daodiscente.persistir(d3);
			daodiscente.persistir(d4);
			DAO.commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	//DAO OK
	public static void cargaServidor() {
		List<TipoParticipacao> tipos = daotipoparticipacao.listar();
		
		try {
			DAO.begin();
			Servidor servidor1 = new Servidor("Servidor 1", 1001, tipos.get(0), true);
			Servidor servidor2 = new Servidor("Servidor 2", 1002, tipos.get(1), false);
			
			System.out.println("Adicionado Servidor: " + servidor1);
			System.out.println("Adicionado Servidor: " + servidor2);

			daoservidor.persistir(servidor1);
			daoservidor.persistir(servidor2);
			DAO.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// DAO OK
	public static void cargaEdital() {
		try {
			DAO.begin();
			Edital edital1 = new Edital("Edital de testes 001");
			Edital edital2 = new Edital("Edital de testes 002");
			
			System.out.println("Cadastrando edital: " + edital1);
			System.out.println("Cadastrando edital: " + edital2);
			daoedital.persistir(edital1);
			daoedital.persistir(edital2);
			DAO.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// DAO OK
	public static void cargaProjeto() {
		List<Edital> editais = daoedital.listar();
		
		try {
			DAO.begin();
			Projeto projeto1 = new Projeto(editais.get(0), "Projeto de testes 1");
			Projeto projeto2 = new Projeto(editais.get(0), "Projeto de testes 2");
			
			System.out.println("Cadastrando projeto: " + projeto1);
			System.out.println("Cadastrando projeto: " + projeto2);
			daoprojeto.persistir(projeto1);
			daoprojeto.persistir(projeto2);
			DAO.commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}



