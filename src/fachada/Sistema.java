package fachada;

import java.util.List;

import javax.persistence.EntityManager;

//import daojpa.*;
import daodb4o.DAO;
import daodb4o.DAOCurso;
import daodb4o.DAODiscente;
import daodb4o.DAOEdital;
import daodb4o.DAOParticipante;
import daodb4o.DAOProjeto;
import daodb4o.DAOServidor;
import daodb4o.DAOTipoParticipacao;
import modelo.Curso;
import modelo.Discente;
import modelo.Edital;
import modelo.Participante;
import modelo.Projeto;
import modelo.Servidor;
import modelo.TipoParticipacao;

public class Sistema {
	
	private static EntityManager manager;
	private static DAODiscente daodiscente = new DAODiscente();
	private static DAOServidor daoservidor = new DAOServidor();
	private static DAOEdital daoedital = new DAOEdital();
	private static DAOProjeto daoprojeto = new DAOProjeto();
	private static DAOParticipante daoparticipante = new DAOParticipante();
	private static DAOCurso daocurso = new DAOCurso();
	private static DAOTipoParticipacao daotipoparticipacao = new DAOTipoParticipacao();

	
	public Sistema() {};
	
	public static void inicializar(){
		DAO.abrir();
	}
	
	public static void fechar(){
		DAO.fechar();
	}
	
	/*
	 * Cadastra um discente
	 */
	public static Discente cadastrarDiscente(String nome, TipoParticipacao tipo, Curso curso, int matricula) throws Exception{
		DAO.begin();
		
		Discente discente = daodiscente.getByMatricula(matricula);
		
		if (discente != null) {
			throw new Exception("Esta matrícula já existe. Você não pode reutiliza-la");
		}
		
		if (nome.length() < 3) {
			throw new Exception("O nome do discente precisa ser maior que 3 caracteres");
		}
		
		if (matricula <= 0) {
			throw new Exception("Por favor, informe uma mátricula maior que 0");
		}
		
		if (curso == null) {
			throw new Exception("Por favor, informe um curso válido");
		}
		
		Discente novo = new Discente(nome, tipo, curso, matricula);
		daodiscente.persistir(novo);
		DAO.commit();

		return novo;
	}

	/*
	 * Cadastra um servidor
	 */
	public static Servidor cadastrarServidor(String nome, int siap, TipoParticipacao tipo, boolean coordenador) throws Exception {
		DAO.begin();
		Servidor servidor = daoservidor.getBySiap(siap);

		if (servidor != null) {
			throw new Exception("Este servidor já existe");
		}

		Servidor novo = new Servidor(nome, siap, tipo, coordenador);
		daoservidor.persistir(novo);
		DAO.commit();

		return novo;
	}
	
	/*
	 * Cadastra um edital
	 */
	public static Edital cadastrarEdital(String nome) throws Exception {
		DAO.begin();
		Edital edital = daoedital.getByNome(nome);
		
		if (edital != null) {
			throw new Exception("Este edital já existe.");
		}
		
		Edital novo = new Edital(nome);
		daoedital.persistir(novo);
		DAO.commit();

		return novo;
	}
	
	/*
	 *  Cadastra um projeto
	 */
	public static Projeto cadastrarProjeto(Edital edital, String titulo) throws Exception {
		DAO.begin();

		if (edital == null) {
			throw new Exception("Sem edital para cadastro..");
		}
		
		Projeto projeto = daoprojeto.getByTituloEEdital(titulo, edital);

		if (projeto != null) {
			throw new Exception("Já existe um projeto neste edital com este nome");
		}
		
		Projeto novo = new Projeto(edital, titulo);
		daoprojeto.persistir(novo);
		DAO.commit();

		return novo;
	}
	
	public static TipoParticipacao cadastrarTipoParticipacao(String nome) throws Exception {
		DAO.begin();

		if (nome.length() == 0) {
			throw new Exception("Informe um nome válido");
		}

		TipoParticipacao tipo = daotipoparticipacao.getByTipo(nome);

		if (tipo != null) {
			throw new Exception("Um tipo de participação com esse nome já existe");
		}

		tipo = new TipoParticipacao(nome);

		daotipoparticipacao.persistir(tipo);
		DAO.commit();

		return tipo;
	}

	/*
	 * Lista todos os participantes
	 */
	public static String listarParticipantes() {
		List<Participante> resultado = daoparticipante.listar();
		
		String saida = "\nListando Participantes\n";
		
		if (resultado.isEmpty()) {
			return saida + "\nNão há participantes cadastrados";
		}
		
		for (Participante p : resultado) {
			if (p instanceof Discente) {
				saida += "[Discente] [Matrícula] " + ((Discente) p).getMatricula() + " - ";
			} else {
				saida += "[Servidor] ";
			}
			saida += p + "\n";

			if (!p.getProjetos().isEmpty()) {
				saida += "["+p.getTipo()+"]";
				saida += "[Projetos]: ";
				for (Projeto projeto : p.getProjetos()) {
					saida += projeto.getTitulo() + "\n";
				}
			}
		}
		
		return saida;
	}
	
	/*
	 * Lista todos os Editais
	 */
	public static String listarEditais() {
		List<Edital> resultado = daoedital.listar();

		String saida = "\nListando Editais\n";
		
		if (resultado.isEmpty()) {
			return saida + "\nNão há editais cadastrados";
		}
		
		for (Edital e : resultado) {
			saida += e + "\n";
		}
		
		return saida;
	}
	
	/*
	 * Lista todos os Projetos
	 */
	public static String listarProjetos() {
		List<Projeto> resultado = daoprojeto.listar();
		
		String saida = "\nListando Projetos\n";
		
		if (resultado.isEmpty()) {
			return saida + "\nNão há projetos cadastrados";
		}
		
		for (Projeto p : resultado) {
			saida += p + "\n";
		}
		return saida;
	}
	
	/*
	 * Lista todos os Cursos
	 */
	public static String listarCursos() {
		List<Curso> resultado = daocurso.listar();
		
		String saida = "\nListando Cursos\n";
		
		if (resultado.isEmpty()) {
			return saida + "\nNão há cursos cadastrados";
		}
		
		for (Curso c : resultado) {
			saida += c + "\n";
		}
		
		return saida;
	}
	
	public static String listarTipoParticipacao() {
		List<TipoParticipacao> resultado = daotipoparticipacao.listar();
		
		String saida = "\nListando Tipos de participação\n";
		
		if (resultado.isEmpty()) {
			saida += "Nenhum tipo cadastrado.\n";
			return saida;
		}
		
		for (TipoParticipacao t : resultado) {
			saida += "[Tipo] " + t + "\n";
		}
		
		return saida;
	}
	
	/*
	 * Busca por um edital
	 */
	public static Edital buscarEdital(String nome) throws Exception {
		if (nome.isEmpty()) {
			throw new Exception("É preciso informar um nome válido");
		}
		
		Edital edital = daoedital.getByNome(nome);
		
		if (edital == null) {
			throw new Exception("Nenhum edital foi encontrado com esse nome.");
		}
		
		return edital;
	}
	
	/*
	 * Busca por um Projeto
	 */
	public static Projeto buscarProjeto(String titulo) throws Exception {
		if (titulo.isEmpty()) {
			throw new Exception("É preciso informar um título válido para o projeto.");
		}
		
		Projeto projeto = daoprojeto.getByTitulo(titulo);
		
		if (projeto == null) {
			throw new Exception("Nenhum projeto foi encontrado com esse título");
		}

		return projeto;
	}
	
	/*
	 * Busca um servidor
	 */
	public static Servidor buscaServidor(int siap) throws Exception {
		Servidor servidor = daoservidor.getBySiap(siap);
		
		if (servidor == null) {
			throw new Exception("Nenhum servidor foi encontrado.");
		}

		return servidor;
	}
	
	/*
	 * Busca um discente
	 */
	public static Discente buscaDiscente(int matricula) throws Exception {
		Discente discente = daodiscente.getByMatricula(matricula);
		
		if (discente == null) {
			throw new Exception("Nenhum discente foi encontrado.");
		}
		
		return discente;
	}
	
	/*
	 * Adiciona participantes a um projeto
	 */
	public static boolean addParticipanteProjeto(Projeto projeto, Participante participante)  throws Exception {

		if (projeto == null) {
			throw new Exception("É preciso informar um projeto válido.");
		}
		
		if (participante == null) {
			throw new Exception("É preciso informar um participante válido.");
		}
		
		for (Participante p : projeto.getParticipantes()) {
			if (p == participante) {
				throw new Exception("Este participante já constitui o quadro do projeto.");
			}
		}

		DAO.begin();
		projeto.addParticipante(participante);
		participante.addProjeto(projeto);
		daoprojeto.atualizar(projeto);
		daoparticipante.atualizar(participante);
		DAO.commit();

		return true;
	}
	
	/*
	 * Remover participante de um projeto
	 */
	
	public static boolean removerParticipanteProjeto(Projeto projeto, Participante participante) throws Exception {
		
		if (projeto == null) {
			throw new Exception("É preciso informar um projeto válido");
		}
		
		if (participante == null) {
			throw new Exception("É preciso informar um participante válido");
		}
		
		for (Participante p : projeto.getParticipantes()) {
			if (p == participante) {
				DAO.begin();
				projeto.removerParticipante(participante);
				daoprojeto.atualizar(projeto);
				DAO.commit();
				return true;
			}
		}

		if (participante != null) {
			throw new Exception("O participante não está no projeto selecionado");	
		}
		
		return false;
	}
	
	/*
	 * Deletar um Edital
	 */
	public static String removerEdital(String nome) throws Exception {
		DAO.begin();
		String saida = "\nRemovendo Edital: " + nome + "\n";
		
		if (nome.length() == 0) {
			throw new Exception("Por favor, informe um nome válido");
		}

		Edital edital = buscarEdital(nome);
		if (edital == null) {
			throw new Exception("Edital não localizado.");
		}
		
		daoedital.apagar(edital);
		DAO.commit();
		saida += "\nEdital removido com sucesso!";
		
		return saida;
	}
	
	public static String removerProjeto(String titulo) throws Exception {
		DAO.begin();
		String saida = "\nRemovendo Projeto: " + titulo + "\n";
		
		if (titulo.length() == 0) {
			throw new Exception("Por favor informe um nome válido");
		}

		Projeto projeto = daoprojeto.getByTitulo(titulo);
		
		if (projeto == null) {
			throw new Exception("Nenhum projeto foi encontrado com esse nome");
		}
		daoprojeto.apagar(projeto);
		DAO.commit();
		saida += "\nProjeto removido com sucesso!";
		
		return saida;
	}
	
	public static void removerDiscente(int matricula) throws Exception {
		DAO.begin();

		Discente discente = daodiscente.getByMatricula(matricula);

		if (discente == null) {
			throw new Exception("Nenhum discente foi encontrado com essa matrícula");
		}

		daodiscente.apagar(discente);
		DAO.commit();
	}
	
	public static Curso cadastrarCurso(String nome, String nivel) throws Exception {
		Curso curso = daocurso.getBynome(nome);
		
		if (curso != null) {
			throw new Exception("Um curso com este nome já existe.");
		}
		
		DAO.begin();
		curso = new Curso(nome, nivel);
		daocurso.persistir(curso);
		DAO.commit();
		
		return curso;
	}
	
	/*
	 * Lista todos os Cursos
	 */
	public static String listarDiscentes() {
		List<Discente> resultado = daodiscente.listar();
		
		String saida = "Listando Discentes\n";
		
		if (resultado.isEmpty()) {
			return saida + "\nNão há cursos cadastrados";
		}
		
		for (Discente d : resultado) {
			saida += d + "\n";
		}
		
		return saida;
	}
}
