package modelo;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(query = "Select d from Discente d where d.matricula = :matricula", name = "find discente by matricula")
public class Discente extends Participante {
	@ManyToOne
	private Curso curso;
	private int matricula;
	public Discente() {}
	
	public Discente(String nome, TipoParticipacao tipo, Curso curso, int matricula) {
		super(nome, tipo);
		this.curso = curso;
		this.matricula = matricula;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public String toString() {
		return this.getNome();
	}
	
}
