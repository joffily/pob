package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/*
 * Classe abstrata para participantes em Projetos
 */

//@MappedSuperclass
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Participante {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome;
	@ManyToOne
	private TipoParticipacao tipo;
	@ManyToMany(mappedBy="participantes", cascade=CascadeType.ALL)
	private List<Projeto> projetos = new ArrayList<Projeto>();
	
	public Participante() {}
	
	public Participante(String nome, TipoParticipacao tipo) {
		this.nome = nome;
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoParticipacao getTipo() {
		return tipo;
	}

	public void setTipo(TipoParticipacao tipo) {
		this.tipo = tipo;
	}

	public List<Projeto> getProjetos() {
		return projetos;
	}

	public void setProjetos(List<Projeto> projetos) {
		this.projetos = projetos;
	}
	
	public void addProjeto(Projeto projeto) {
		this.projetos.add(projeto);
	}

}
