package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;

//import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQuery(query = "Select e from Edital e where e.nome = :nome", name = "find edital by name")
public class Edital {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome;
	private int total_bolsas_docente;
	private int total_bolsas_discente;
	private double valor_bolsa_docente;
	private double valor_bolsa_discente;
	@OneToMany(mappedBy="edital", cascade=CascadeType.REMOVE, orphanRemoval=true)
	private List<Projeto> projetos = new ArrayList<Projeto>();
	
	public Edital() {}

	public Edital(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getTotal_bolsas_docente() {
		return total_bolsas_docente;
	}

	public void setTotal_bolsas_docente(int total_bolsas_docente) {
		this.total_bolsas_docente = total_bolsas_docente;
	}

	public int getTotal_bolsas_discente() {
		return total_bolsas_discente;
	}

	public void setTotal_bolsas_discente(int total_bolsas_discente) {
		this.total_bolsas_discente = total_bolsas_discente;
	}

	public double getValor_bolsa_docente() {
		return valor_bolsa_docente;
	}

	public void setValor_bolsa_docente(double valor_bolsa_docente) {
		this.valor_bolsa_docente = valor_bolsa_docente;
	}

	public double getValor_bolsa_discente() {
		return valor_bolsa_discente;
	}

	public void setValor_bolsa_discente(double valor_bolsa_discente) {
		this.valor_bolsa_discente = valor_bolsa_discente;
	}
	
	public List<Projeto> getProjetos() {
		return projetos;
	}

	@Override
	public String toString() {
		return this.nome;
	}

	public int getId() {
		return this.id;
	}
}
