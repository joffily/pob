package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TipoParticipacao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome;
	@OneToMany(mappedBy="tipo", cascade=CascadeType.ALL, targetEntity=Participante.class)
	private List<Participante> participantes = new ArrayList<Participante>();
	

	public TipoParticipacao() {}
	
	public TipoParticipacao(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return this.nome;
	}

	public List<Participante> getParticipantes() {
		return this.participantes;
	}
}
