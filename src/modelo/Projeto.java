package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(query = "Select p from Projeto p where p.titulo = :titulo", name = "find projeto by title")
public class Projeto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne
	private Edital edital;
	private String titulo;
	@ManyToMany(cascade=CascadeType.REMOVE)
	private List<Participante> participantes = new ArrayList<Participante>();
	
	public Projeto() {}
	
	public Projeto(Edital edital, String titulo) {
		this.edital = edital;
		this.titulo = titulo;
	}

	public Edital getEdital() {
		return edital;
	}

	public void setEdital(Edital edital) {
		this.edital = edital;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public List<Participante> getParticipantes() {
		return participantes;
	}

	public void addParticipante(Participante participante) {
		this.participantes.add(participante);
	}
	
	public boolean removerParticipante(Participante participante) {
		for (Participante p : this.participantes) {
			if (p == participante) {
				this.participantes.remove(participante);
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		return "Projeto: " + this.titulo;
	}
	

}
