package modelo;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(query = "Select s from Servidor s where s.siap = :siap", name = "find servidor by siap")
public class Servidor extends Participante {
	private boolean coordenador;
	private int siap;
	
	public Servidor() {}
	
	public Servidor(String nome, int siap, TipoParticipacao tipo, boolean coordenador) {
		super(nome, tipo);
		this.siap = siap;
		this.coordenador = coordenador;
	}

	public boolean isCoordenador() {
		return coordenador;
	}

	public void setCoordenador(boolean coordenador) {
		this.coordenador = coordenador;
	}

	public String toString() {
		if (this.isCoordenador()) {
			return "[Coordenador] " + this.getNome() + ", SIAP: " + this.siap;	
		}

		return this.getNome() + ", SIAP: " + this.siap;
		
	}
	
}
