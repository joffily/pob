package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.Curso;

public class DAOCurso extends DAO<Curso> {
	public DAOCurso() {
		super();
	}
	
	public Curso getBynome(String nome) {
		Query qcurso = manager.createQuery("select c from Curso c where c.nome='"+nome+"'");
		List<Curso> cursos = qcurso.getResultList();
		
		if (cursos.isEmpty()) {
			return null;
		}
		
		return cursos.get(0);
	}
}
