package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.Edital;
import modelo.Projeto;

public class DAOProjeto extends DAO<Projeto> {

	public DAOProjeto() {
		super();
	}
	
	public Projeto getByTituloEEdital(String titulo, Edital edital) {
		Query q = manager.createQuery("select p from Projeto p where p.titulo LIKE '"+titulo+"' and p.edital.nome LIKE '"+edital.getNome()+"'");
		List<Projeto> projetos = q.getResultList();
		
		if (projetos.isEmpty()) {
			return null;
		}

		return projetos.get(0);
	}
	
	public Projeto getByTitulo(String titulo) {
		Query q = manager.createNamedQuery("find projeto by title");
		q.setParameter("titulo", titulo);
		List<Projeto> projetos = q.getResultList();
		
		if (projetos.isEmpty()) {
			return null;
		}
		
		return projetos.get(0);
	}
}
