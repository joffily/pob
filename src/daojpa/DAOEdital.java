package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.Edital;

public class DAOEdital extends DAO<Edital> {

	public DAOEdital() {
		super();
	}
	
	public Edital getByNome(String nome) {
		Query q = manager.createNamedQuery("find edital by name");
		q.setParameter("nome", nome);
		@SuppressWarnings("unchecked")
		List<Edital> editais = q.getResultList();
		
		if (editais.isEmpty()) {
			return null; 
		}

		return editais.get(0);
	}
}
