package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.Servidor;

public class DAOServidor extends DAO<Servidor> {

	public DAOServidor() {
		super();
	}
	
	public Servidor getBySiap(int siap) {
		Query q = manager.createQuery("select s from Servidor s where s.siap='"+siap+"'");
		@SuppressWarnings("unchecked")
		List<Servidor> servidores = q.getResultList();
		
		if (servidores.isEmpty()) {
			return null;
		}
		
		return servidores.get(0);
	}
}
