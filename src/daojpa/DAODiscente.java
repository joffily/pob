package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.Discente;

public class DAODiscente extends DAO<Discente> {
	
	public DAODiscente() {
		super();
	}

	public Discente getByMatricula(int matricula) {
		Query q = manager.createQuery("select d from Discente d where d.matricula='"+matricula+"'");
		List<Discente> discentes = q.getResultList();

		if (discentes.isEmpty()) {
			return null;
		}
		
		return discentes.get(0);
	}
}
