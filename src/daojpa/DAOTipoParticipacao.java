package daojpa;

import java.util.List;

import javax.persistence.Query;

import modelo.TipoParticipacao;

public class DAOTipoParticipacao extends DAO<TipoParticipacao> {
	public DAOTipoParticipacao() {
		super();
	}
	
	public TipoParticipacao getByTipo(String tipo) {
		Query qtipo = manager.createQuery("select t from TipoParticipacao t where t.nome='"+tipo+"'");
		List<TipoParticipacao> tipos = qtipo.getResultList();
		
		if (tipos.isEmpty()) {
			return null;
		}
		
		return tipos.get(0);
	}
}
