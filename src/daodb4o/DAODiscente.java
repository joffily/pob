package daodb4o;

import java.util.List;
import com.db4o.query.Query;

import modelo.Discente;

public class DAODiscente extends DAO<Discente> {
	public DAODiscente() {
		super();
	}

	public Discente getByMatricula(int matricula) {
		try{
			Query q = manager.query();
			q.constrain(Discente.class);
			q.descend("matricula").constrain(matricula);
			List<Discente> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
}
