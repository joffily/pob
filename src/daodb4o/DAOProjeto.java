package daodb4o;
import java.util.List;
import com.db4o.query.Query;
import modelo.Edital;
import modelo.Projeto;

public class DAOProjeto extends DAO<Projeto> {

	public Projeto getByTituloEEdital(String titulo, Edital edital) {
		try{
			Query q = manager.query();
			q.constrain(Projeto.class);
			q.descend("titulo").constrain(titulo);
			q.descend("edital").constrain(edital);
			
			List<Projeto> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
	
	public Projeto getByTitulo(String titulo) {
		try{
			Query q = manager.query();
			q.constrain(Projeto.class);
			q.descend("titulo").constrain(titulo);

			List<Projeto> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}

}
