package daodb4o;

import java.util.List;

import com.db4o.query.Query;

import modelo.Edital;

public class DAOEdital extends DAO<Edital> {
	public DAOEdital() {
		super();
	}
	
	public Edital getByNome(String nome) {
		try{
			Query q = manager.query();
			q.constrain(Edital.class);
			q.descend("nome").constrain(nome);
			List<Edital> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
}
