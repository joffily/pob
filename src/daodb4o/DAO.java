/**IFPB - Curso SI - Disciplina de POB
 * @author Prof Fausto Ayres
 */

package daodb4o;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.constraints.UniqueFieldValueConstraintViolationException;

import modelo.Curso;
import modelo.Discente;
import modelo.Edital;
import modelo.Participante;
import modelo.Projeto;
import modelo.Servidor;
import modelo.TipoParticipacao;


public abstract class DAO<T> implements DAOInterface<T> {
	protected static ObjectContainer manager;

	public static void abrir(){
		if(manager==null){
			
			EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
			config.common().messageLevel(0);   //0,1,2,3,4

			config.common().objectClass(Curso.class).cascadeOnUpdate(true);
			config.common().objectClass(Curso.class).cascadeOnDelete(true);
			config.common().objectClass(Curso.class).cascadeOnActivate(true);

			config.common().objectClass(Discente.class).cascadeOnUpdate(true);
			config.common().objectClass(Discente.class).cascadeOnDelete(true);
			config.common().objectClass(Discente.class).cascadeOnActivate(true);

			config.common().objectClass(Edital.class).cascadeOnUpdate(true);
			config.common().objectClass(Edital.class).cascadeOnDelete(true);
			config.common().objectClass(Edital.class).cascadeOnActivate(true);

			config.common().objectClass(Participante.class).cascadeOnUpdate(true);
			config.common().objectClass(Participante.class).cascadeOnDelete(true);
			config.common().objectClass(Participante.class).cascadeOnActivate(true);

			config.common().objectClass(Projeto.class).cascadeOnUpdate(true);
			config.common().objectClass(Projeto.class).cascadeOnDelete(true);
			config.common().objectClass(Projeto.class).cascadeOnActivate(true);

			config.common().objectClass(Servidor.class).cascadeOnUpdate(true);
			config.common().objectClass(Servidor.class).cascadeOnDelete(true);
			config.common().objectClass(Servidor.class).cascadeOnActivate(true);

			config.common().objectClass(TipoParticipacao.class).cascadeOnUpdate(true);
			config.common().objectClass(TipoParticipacao.class).cascadeOnDelete(true);
			config.common().objectClass(TipoParticipacao.class).cascadeOnActivate(true);

 			//indexacao de atributos para agilizar a busca
			config.common().objectClass(Curso.class).objectField("id").indexed(true);
			config.common().objectClass(Discente.class).objectField("id").indexed(true);
			config.common().objectClass(Edital.class).objectField("id").indexed(true);
			config.common().objectClass(Participante.class).objectField("id").indexed(true);
			config.common().objectClass(Projeto.class).objectField("id").indexed(true);
			config.common().objectClass(Servidor.class).objectField("id").indexed(true);
			config.common().objectClass(TipoParticipacao.class).objectField("id").indexed(true);
			manager = Db4oEmbedded.openFile(config, "banco.db4o");
			
			//--------------Inicializar o gerente de id-----------------
			//Apos a cria��o do banco
			AutoGenerateIDManager.inicializar(manager);
			//----------------------------------------------------------
		}
	}

	public static void fechar(){
		if(manager!=null) {
			manager.close();
			manager=null;
		}
	}

	@Override
	public void persistir(T obj){
		manager.store( obj );
	}
	
	@Override
	public T localizar(Object chave){
		return null;
	};  //implementado nos dao especificos
	
	@Override
	public T atualizar(T obj){
		manager.store(obj);
		return obj;
	}
	@Override
	public void apagar(T obj) {
		manager.delete(obj);
	}
	@Override
	public void reler(T obj){
		manager.ext().refresh(obj, Integer.MAX_VALUE);
	}
	
	//--------transa��o---------------
	public static void begin(){	}		// tem que ser vazio!
	
	public static void commit(){
		try {
			manager.commit();
		} 
		// Evitar de gravar objetos duplicados
		catch (UniqueFieldValueConstraintViolationException e) {
			throw new RuntimeException ("atributo duplicado  " + e.getMessage());
		}
	}
	public static void flush(){	//commit intermediario
		commit();
	}
	public static void rollback(){
		manager.rollback();
	}
	@SuppressWarnings("unchecked")
	public List<T> listar(){
		Class<T> type = (Class<T>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
		return (List<T>) manager.query(type);
	}

	
}

