package daodb4o;

import modelo.Edital;
import modelo.Servidor;

import java.util.List;

import com.db4o.query.Query;

public class DAOServidor extends DAO<Servidor> {
	
	public DAOServidor() {
		super();
	}

	public Servidor getBySiap(int siap) {
		try{
			Query q = manager.query();
			q.constrain(Servidor.class);
			q.descend("siap").constrain(siap);
			List<Servidor> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
}
