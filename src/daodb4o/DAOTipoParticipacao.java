package daodb4o;
import java.util.List;

import com.db4o.query.Query;

import modelo.TipoParticipacao;

public class DAOTipoParticipacao extends DAO<TipoParticipacao> {
	public DAOTipoParticipacao() {
		super();
	}

	public TipoParticipacao getByTipo(String tipo) {
		try{
			Query q = manager.query();
			q.constrain(TipoParticipacao.class);
			q.descend("nome").constrain(tipo);
			List<TipoParticipacao> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
	
}
