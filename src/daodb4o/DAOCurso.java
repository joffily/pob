package daodb4o;

import java.util.List;

import com.db4o.query.Query;

import modelo.Curso;

public class DAOCurso extends DAO<Curso> {
	public DAOCurso() {
		super();
	}

	public Curso getBynome(String nome) {
		try{
			Query q = manager.query();
			q.constrain(Curso.class);
			q.descend("nome").constrain(nome);
			List<Curso> resultados = q.execute();
			if (resultados.size()>0)
				return resultados.get(0);
			else
				return null;
		}
		catch(ClassCastException e){
			throw new RuntimeException("campo de busca invalido");
		}
	}
}
