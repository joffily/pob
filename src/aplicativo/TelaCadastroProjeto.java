package aplicativo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

//import daojpa.*;
import daodb4o.*;
import fachada.Sistema;
import modelo.Edital;
import modelo.Projeto;

public class TelaCadastroProjeto extends AlterarDB {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblNome;
	private JButton btnCriar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroProjeto frame = new TelaCadastroProjeto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaCadastroProjeto() {

		setTitle("Cadastrar Projeto");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 147);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(98, 11, 188, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		lblNome = new JLabel("Título");
		lblNome.setBounds(10, 14, 78, 14);
		contentPane.add(lblNome);

		JLabel lblEdital = new JLabel("Edital");
		lblEdital.setBounds(10, 53, 78, 14);
		contentPane.add(lblEdital);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(98, 48, 188, 27);
		for (Edital e: daoedital.listar()) {
			comboBox.addItem(e);	
		}
		contentPane.add(comboBox);

		btnCriar = new JButton("Cadastrar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Projeto p = Sistema.cadastrarProjeto((Edital)comboBox.getSelectedItem(), textField.getText());
					JOptionPane.showMessageDialog(null, "Projeto cadastrado com sucesso");
					textField.setText("");
					textField.requestFocus();
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(119, 87, 115, 23);
		contentPane.add(btnCriar);

	}
}
