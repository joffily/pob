package aplicativo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import fachada.Sistema;
import modelo.Curso;

@SuppressWarnings("serial")
public class TelaCadastroDiscente extends AlterarDB {
	private JPanel contentPane;
	private JTextField matriculaField;
	private JLabel lblNome;
	private JButton btnCriar;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBox;
	private JLabel lblNome_2;
	private JTextField nomeField;
	private JLabel lblCurso_1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroDiscente frame = new TelaCadastroDiscente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TelaCadastroDiscente() {
		setTitle("Cadastrar Discente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		matriculaField = new JTextField();
		matriculaField.setBounds(98, 11, 188, 20);
		contentPane.add(matriculaField);
		matriculaField.setColumns(10);

		lblNome = new JLabel("Matrícula");
		lblNome.setBounds(10, 14, 78, 14);
		contentPane.add(lblNome);

		lblNome_2 = new JLabel("Nome");
		lblNome_2.setBounds(10, 46, 78, 14);
		contentPane.add(lblNome_2);
		
		nomeField = new JTextField();
		nomeField.setColumns(10);
		nomeField.setBounds(98, 43, 188, 20);
		contentPane.add(nomeField);
		
		lblCurso_1 = new JLabel("Curso");
		lblCurso_1.setBounds(10, 72, 78, 14);
		contentPane.add(lblCurso_1);
		
		comboBox = new JComboBox();
		comboBox.setBounds(98, 75, 188, 27);
		contentPane.add(comboBox);
		
		for (Curso c : daocurso.listar()) {
			comboBox.addItem(c);
		}
		
		btnCriar = new JButton("Cadastrar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int matricula;
					if (matriculaField.getText().equals("")) {
						matricula = 0;
					} else {
						matricula = Integer.parseInt(matriculaField.getText());						
					}
					String nome = nomeField.getText();
					Curso curso = (Curso)comboBox.getSelectedItem();
					Sistema.cadastrarDiscente(nome, null, curso, matricula);
					JOptionPane.showMessageDialog(null,"Discente cadastrado");
					matriculaField.setText("");
					matriculaField.requestFocus();
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(98, 109, 188, 23);
		contentPane.add(btnCriar);
	}
}
