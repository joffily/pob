package aplicativo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import daojpa.DAO;
import daojpa.DAOParticipante;
import daojpa.DAOProjeto;
import daojpa.DAOTipoParticipacao;
import fachada.Sistema;
import modelo.Participante;
import modelo.Projeto;
import modelo.TipoParticipacao;

public class TelaCadastroProjetoParticipante extends AlterarDB {

	private JPanel contentPane;
	private JButton btnCriar;
	private JLabel lblParticipante;
	private JComboBox participanteBox;
	private JLabel lblTipoDeParticipao;
	private JComboBox participacaoBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaCadastroProjetoParticipante frame = new TelaCadastroProjetoParticipante();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public TelaCadastroProjetoParticipante() {
		setTitle("Adicionar Participante a Projeto");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEdital = new JLabel("Projeto");
		lblEdital.setBounds(6, 19, 78, 14);
		contentPane.add(lblEdital);

		JComboBox projetoBox = new JComboBox();
		projetoBox.setBounds(98, 14, 188, 27);
		for (Projeto e: daoprojeto.listar()) {
			projetoBox.addItem(e);	
		}
		contentPane.add(projetoBox);
		
		lblParticipante = new JLabel("Participante");
		lblParticipante.setBounds(6, 53, 78, 14);
		contentPane.add(lblParticipante);
		
		participanteBox = new JComboBox();
		participanteBox.setBounds(98, 48, 188, 27);
		contentPane.add(participanteBox);
		
		lblTipoDeParticipao = new JLabel("Participação");
		lblTipoDeParticipao.setBounds(6, 94, 78, 14);
		contentPane.add(lblTipoDeParticipao);
		
		participacaoBox = new JComboBox();
		participacaoBox.setBounds(98, 87, 188, 27);
		contentPane.add(participacaoBox);
		
		for (Participante p : daoparticipante.listar()) {
			participanteBox.addItem(p);
		}
		
		for (TipoParticipacao t : daotipoparticipacao.listar()) {
			participacaoBox.addItem(t);
		}
		
		btnCriar = new JButton("Cadastrar");
		btnCriar.setBounds(119, 132, 115, 23);
		contentPane.add(btnCriar);

		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Projeto projeto = (Projeto)projetoBox.getSelectedItem();
					Participante participante = (Participante)participanteBox.getSelectedItem();
					TipoParticipacao tipo = (TipoParticipacao)participacaoBox.getSelectedItem();
					participante.setTipo(tipo);
//					DAO.begin();
					daoparticipante.atualizar(participante);
//					DAO.commit();
					Sistema.addParticipanteProjeto(projeto, participante);
					JOptionPane.showMessageDialog(null, "Participante Adicionado ao projeto com sucesso!");
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
	}
}
