package aplicativo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fachada.Sistema;
import modelo.Edital;
import modelo.Projeto;

public class TelaRemoverProjeto extends AlterarDB {
	private JPanel contentPane;
	private JButton btnCriar;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaRemoverProjeto frame = new TelaRemoverProjeto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public TelaRemoverProjeto() {
		setTitle("Remover Projeto");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 147);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEdital = new JLabel("Projeto");
		lblEdital.setBounds(10, 14, 78, 14);
		contentPane.add(lblEdital);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(98, 11, 188, 20);
		for (Projeto e: daoprojeto.listar()) {
			comboBox.addItem(e);	
		}
		contentPane.add(comboBox);

		btnCriar = new JButton("Remover");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Projeto objeto = (Projeto)comboBox.getSelectedItem();
					if (objeto == null) {
						Sistema.removerProjeto("");
					} else {
						Sistema.removerProjeto(objeto.getTitulo());	
					}
					JOptionPane.showMessageDialog(null, "Projeto removido com sucesso.");
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(119, 87, 115, 23);
		contentPane.add(btnCriar);
	}
}
