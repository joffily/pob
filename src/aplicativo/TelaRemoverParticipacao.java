package aplicativo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fachada.Sistema;
import modelo.Discente;
import modelo.Projeto;

public class TelaRemoverParticipacao extends AlterarDB {

	public TelaRemoverParticipacao() {
		JPanel contentPane;
		JButton btnCriar;
		setTitle("Remover Particiapação");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 147);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEdital = new JLabel("Discente");
		lblEdital.setBounds(10, 14, 78, 14);
		contentPane.add(lblEdital);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(98, 14, 188, 20);
		for (Discente d: daodiscente.listar()) {
			comboBox.addItem(d);	
		}
		contentPane.add(comboBox);
		
		JLabel labelProjeto = new JLabel("Projeto");
		labelProjeto.setBounds(10, 50, 298, 14);
		contentPane.add(labelProjeto);
		
		JComboBox boxProjeto = new JComboBox();
		boxProjeto.setBounds(98, 50, 188, 20);
		for (Projeto p: daoprojeto.listar()) {
			boxProjeto.addItem(p);	
		}
		contentPane.add(boxProjeto);

		btnCriar = new JButton("Remover");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Discente discente = (Discente)comboBox.getSelectedItem();
					Projeto projeto = (Projeto)boxProjeto.getSelectedItem();
					
					Sistema.removerParticipanteProjeto(projeto, discente);

					JOptionPane.showMessageDialog(null, "Discente removido com sucesso.");
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(119, 87, 115, 23);
		contentPane.add(btnCriar);
	}

}
