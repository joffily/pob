package aplicativo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fachada.Sistema;
import modelo.Discente;
import modelo.Edital;

public class TelaRemoverDiscente extends AlterarDB {
	
	public TelaRemoverDiscente() {
		JPanel contentPane;
		JButton btnCriar;
		setTitle("Remover Discente");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 311, 147);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblEdital = new JLabel("Discente");
		lblEdital.setBounds(10, 14, 78, 14);
		contentPane.add(lblEdital);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(98, 11, 188, 20);
		for (Discente d: daodiscente.listar()) {
			comboBox.addItem(d);	
		}
		contentPane.add(comboBox);

		btnCriar = new JButton("Remover");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					Discente objeto = (Discente)comboBox.getSelectedItem();
					if (objeto == null) {
						Sistema.removerDiscente(0);
					} else {
						Sistema.removerDiscente(objeto.getMatricula());	
					}

					JOptionPane.showMessageDialog(null, "Discente removido com sucesso.");
				}
				catch(Exception erro){
					JOptionPane.showMessageDialog(null,erro.getMessage());
				}
			}
		});
		btnCriar.setBounds(119, 87, 115, 23);
		contentPane.add(btnCriar);

	}
}
