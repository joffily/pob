package aplicativo;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import fachada.Sistema;

public class Principal {

	private JFrame frmPrincipal;
	private JMenuItem mntmCadastrar;
	private JMenuItem mntmListar;
	private JMenu mnEdital;
	private JMenu mnProjeto;
	private JMenuItem submenu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmPrincipal.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPrincipal = new JFrame();
		frmPrincipal.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Sistema.inicializar();
//				JOptionPane.showMessageDialog(null, "sistema inicializado !");
			}
			@Override
			public void windowClosing(WindowEvent e) {
				Sistema.fechar();
//				JOptionPane.showMessageDialog(null, "sistema finalizado !");
			}
		});
		frmPrincipal.setTitle("DPPE Online");
		frmPrincipal.setBounds(100, 100, 600, 300);
		frmPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrincipal.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		frmPrincipal.setJMenuBar(menuBar);
		
		// Edital
		JMenu edital = this.addMenu("Edital"); 
		menuBar.add(edital);

		// Listar Edital
		mntmListar = new JMenuItem("Listar");
		mntmListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarEdital j = new TelaListarEdital();
				j.setVisible(true);
			}
		});
		edital.add(mntmListar);
		
		// Cadastro Edital
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroEdital j = new TelaCadastroEdital();
				j.setVisible(true);
			}
		});
		edital.add(submenu);
		
		/*
		 * Projeto
		 */
		JMenu projeto = this.addMenu("Projeto"); 
		menuBar.add(projeto);
		// Listar
		submenu = new JMenuItem("Listar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarProjeto j = new TelaListarProjeto();
				j.setVisible(true);
			}
		});
		projeto.add(submenu);

		// Cadastrar projeto
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroProjeto j = new TelaCadastroProjeto();
				j.setVisible(true);
			}
		});
		projeto.add(submenu);

		/*
		 * Curso
		 */
		JMenu curso = this.addMenu("Curso");
		menuBar.add(curso);
		
		// Listar
		submenu = new JMenuItem("Listar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarCurso j = new TelaListarCurso();
				j.setVisible(true);
			}
		});
		curso.add(submenu);
		
		// Cadastro curso
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroCurso j = new TelaCadastroCurso();
				j.setVisible(true);
			}
		});
		curso.add(submenu);
		
		/*
		 * Discente
		 */
		JMenu discente = this.addMenu("Discente");
		menuBar.add(discente);

		// Listar
		submenu = new JMenuItem("Listar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarDiscente j = new TelaListarDiscente();
				j.setVisible(true);
			}
		});
		discente.add(submenu);
		// Cadastro Discente
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroDiscente j = new TelaCadastroDiscente();
				j.setVisible(true);
			}
		});
		discente.add(submenu);
		/*
		 * Tipo de participação
		 */
		JMenu tipo = this.addMenu("Tipo Part.");
		menuBar.add(tipo);
		
		// Listar
		submenu = new JMenuItem("Listar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarTipo j = new TelaListarTipo();
				j.setVisible(true);
			}
		});
		tipo.add(submenu);

		// Cadastro Participacao em projeto
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroTipoParticipacao j = new TelaCadastroTipoParticipacao();
				j.setVisible(true);
			}
		});
		tipo.add(submenu);
		
		/*
		 * Participante
		 */
		JMenu participante = this.addMenu("Participante");
		menuBar.add(participante);

		// Listar
		submenu = new JMenuItem("Listar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaListarParticipante j = new TelaListarParticipante();
				j.setVisible(true);
			}
		});
		participante.add(submenu);

		// Cadastro Participacao em projeto
		submenu = new JMenuItem("Cadastrar");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaCadastroProjetoParticipante j = new TelaCadastroProjetoParticipante();
				j.setVisible(true);
			}
		});
		participante.add(submenu);

		/*
		 * Remoção
		 */
		JMenu remover = this.addMenu("Remoção");
		menuBar.add(remover);
		
		// Remover Edital
		submenu = new JMenuItem("Edital");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaRemoverEdital j = new TelaRemoverEdital();
				j.setVisible(true);
			}
		});
		remover.add(submenu);
		
		// Remover Projeto
		submenu = new JMenuItem("Projeto");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaRemoverProjeto j = new TelaRemoverProjeto();
				j.setVisible(true);
			}
		});
		remover.add(submenu);
		
		// Remover Discente
		submenu = new JMenuItem("Discente");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaRemoverDiscente j = new TelaRemoverDiscente();
				j.setVisible(true);
			}
		});
		remover.add(submenu);

		// Remover Participacao
		submenu = new JMenuItem("Participacao");
		submenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaRemoverParticipacao j = new TelaRemoverParticipacao();
				j.setVisible(true);
			}
		});
		remover.add(submenu);
	}
	
	
	public JMenu addMenu(String nome) {
		JMenu menu = new JMenu(nome);
		return menu;
	}

}
