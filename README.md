# Projeto da Disciplina de Persistência de Objetos 2016.1
# Autor: Joffily Ferreira

## Classes para testes
- Cadastro
- Atualização
- Consulta

## Classe de fachada
- Sistema

## Classes entidades
- Edital
- Projeto
- Participação
- Discente
- Servidor
- TipoParticipação
- Curso